'use strict';

const gulp = require ('gulp');
const path = require ('path');
const pug = require ('gulp-pug');
const sass = require('gulp-sass');
sass.compiler = require('node-sass');
const browserSync = require ('browser-sync').create();
const watch = require ('gulp-watch');
const sourcemaps = require ('gulp-sourcemaps');
const gulpIf = require ('gulp-if');
const del = require ('del');
const imagemin = require ('gulp-imagemin');
const cleanCSS = require ('gulp-clean-css');
const browserify = require('browserify');
const babelify = require('babelify');
const babel = require('gulp-babel');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

gulp.task('buildHTML', ()=>{
  return gulp.src('src/*.pug')
                .pipe (gulpIf(isDevelopment, sourcemaps.init()))
                .pipe (pug({
                    pretty: true
                  }))
                .pipe (gulpIf(isDevelopment, sourcemaps.write()))
                .pipe (gulp.dest('dist'))

});

gulp.task('buildInclude', ()=>{
  return gulp.src('src/includes/*.pug')
    .pipe (gulpIf(isDevelopment, sourcemaps.init()))
    .pipe (pug({
      pretty: true
    }))
    .pipe (gulpIf(isDevelopment, sourcemaps.write()))
    .pipe (gulp.dest('dist/includes'))

});

gulp.task('buildSass', function () {
  return gulp.src(['src/styles/*.scss', '!src/styles/_*.scss', '!src/styles/**/_*.scss'])
    .pipe(sass({
      includePaths: require('node-normalize-scss').includePaths,
      importer: require('npm-sass').importer
    }))
    .pipe(gulpIf(isDevelopment, sourcemaps.write()))
    .pipe(gulp.dest('dist/styles'));
});

gulp.task('sassMin', function () {
  return gulp.src(['src/styles/*.scss', '!src/styles/_*.scss', '!src/styles/**/_*.scss'])
    .pipe(sass({
      includePaths: require('node-normalize-scss').includePaths,
      importer: require('npm-sass').importer
    }))
    .pipe(cleanCSS({
      level: 2}))
    .pipe(gulpIf(isDevelopment, sourcemaps.write()))
    .pipe(gulp.dest('dist/styles'));
});

gulp.task('buildJS', ()=>{
  return browserify({entries: ['src/js/index.js'], debug: true})
    .transform("babelify", { presets: ["@babel/preset-env"] })
    .bundle()
    .pipe(source('index.js'))
    .pipe(gulp.dest('dist/js'))
    .pipe (browserSync.stream());
});

gulp.task('buildAssets', ()=> {
  return gulp.src('src/assets/**/**/*.*')
                .pipe(gulp.dest('dist/assets'))
});

gulp.task('imageMin', ()=> {
  return gulp.src('src/assets/**/*.*')
                .pipe(imagemin({
                progressive: true,
                svgoPlugins: [{removeViewBox: false}],
                interlaced: true
              }))
                .pipe(gulp.dest('dist/assets'))
});

gulp.task('clean', ()=>{
  return del('dist');
});

gulp.task('buildLocal', gulp.series('clean', gulp.parallel('buildHTML', 'buildInclude', 'buildSass', 'buildAssets', 'buildJS')));

gulp.task('watch', ()=>{
  gulp.watch('./src/js/**/*.js', gulp.series('buildJS'));
  gulp.watch('./src/styles/**/*.scss', gulp.series('buildSass'));
  gulp.watch('./src/assets/**/**/*.*', gulp.series('buildAssets'));
  gulp.watch('./src/**/**/*.pug', gulp.series('buildHTML'))
});

gulp.task('serve', () => {
  browserSync.init({
    server: "dist"
  });
  browserSync.watch('dist/**/**/*.*').on('change', browserSync.reload);
});

gulp.task('local',
  gulp.series('buildLocal', gulp.parallel('watch', 'serve'))
);

gulp.task('prod', gulp.series('clean', gulp.parallel('buildHTML', 'buildInclude', 'sassMin', 'imageMin', 'buildJS')));


