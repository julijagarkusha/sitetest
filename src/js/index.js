import menuClick from "./utils/menuClick";
import dotsClick from "./utils/dotsClick";

const menuItems = document.querySelectorAll(".menu__item");
menuItems.forEach(menuItem => {
  menuItem.addEventListener("click", menuClick)
});

const dotsElements = document.querySelectorAll(".dots__item");
dotsElements.forEach(dotsElement => {
  dotsElement.addEventListener("click", dotsClick)
});