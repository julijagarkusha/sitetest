const dotsClick = (event) => {
  const target = event.target;

  const dotsElem = document.querySelectorAll(".dots__item");
  dotsElem.forEach(dotElem => {
    dotElem.classList.remove("dots__item--active")
  });

  const slides = document.querySelectorAll(".slider__item");
  slides.forEach(slider => {
    slider.classList.remove("slider__item--active");
    if (slider.getAttribute("data-value") === target.getAttribute("data-value")) {
      slider.classList.add("slider__item--active")
    }
  });
  target.classList.add("dots__item--active");

};

export default dotsClick;
