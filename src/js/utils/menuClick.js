const menuClick = (event) => {
  const target = event.target;
  const targetParent = target.parentNode;
  const menuItems = document.querySelectorAll(".menu__item");

  menuItems.forEach(menuItem => {
    menuItem.classList.remove("menu__item--active")
  });

  if (target.classList.contains("menu__item")) {
    target.classList.add("menu__item--active")
  } else {
    targetParent.classList.add("menu__item--active")
  }

};

export default menuClick;